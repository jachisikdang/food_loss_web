// react
import React from 'react';

// third-party
import { Helmet } from 'react-helmet';

// application
import PageHeader from '../shared/PageHeader';

// blocks
import BlockMap from '../blocks/BlockMap';

// data stubs
import theme from '../../data/theme';


function SitePageContactUs() {
    const breadcrumb = [
        { title: '나눠먹자', url: '' },
        { title: '고객센터', url: '' },
    ];

    return (
        <React.Fragment>
            <Helmet>
                <title>{`고객센터 — ${theme.name}`}</title>
            </Helmet>

            <BlockMap />

            <PageHeader header="고객센터" breadcrumb={breadcrumb} />

            <div className="block">
                <div className="container">
                    <div className="card mb-0">
                        <div className="card-body contact-us">
                            <div className="contact-us__container">
                                <div className="row">
                                    <div className="col-12 col-lg-6 pb-4 pb-lg-0">
                                        <h4 className="contact-us__header card-title">나눠먹자 주소</h4>

                                        <div className="contact-us__address">
                                            <p>
                                              <strong>주소</strong>
                                              <br/>
                                              서울특별시 마포구 월드컵북로 481
                                              <br/>
                                              <br/>

                                              <strong>이메일</strong>
                                              <br/>
                                              jachisikdang@gmail.com
                                              <br/>
                                              <br/>

                                              <strong>대표 번호</strong>
                                              <br />
                                                010-5159-3850
                                            </p>

                                            <p>
                                                <strong>영업 시간</strong>
                                                <br />
                                                월-토 10:00pm - 7:00pm
                                            </p>


                                        </div>
                                    </div>

                                    <div className="col-12 col-lg-6">
                                        <h4 className="contact-us__header card-title">메시지를 남겨주세요</h4>

                                        <form>
                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <label htmlFor="form-name">이름</label>
                                                    <input type="text" id="form-name" className="form-control" placeholder="Your Name" />
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label htmlFor="form-email">이메일</label>
                                                    <input
                                                        type="email"
                                                        id="form-email"
                                                        className="form-control"
                                                        placeholder="Email Address"
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="form-subject">제목</label>
                                                <input type="text" id="form-subject" className="form-control" placeholder="Subject" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="form-message">메시지</label>
                                                <textarea id="form-message" className="form-control" rows="4" />
                                            </div>
                                            <button type="submit" className="btn btn-primary">메시지 보내기</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default SitePageContactUs;
