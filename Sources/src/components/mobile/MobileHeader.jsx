// react
import React, { Component } from 'react';

// third-party
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';

// reducer
import * as mobileMenuActions from '../../redux/modules/mobileMenu';

// application
import Indicator from '../header/Indicator';
import {
    Menu18x14Svg,
    LogoSmallSvg,
    Search20Svg,
    Cross20Svg,
    Heart20Svg,
    Cart20Svg,
} from '../../svg';


class MobileHeader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchOpen: false,
        };
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleOutsideClick);
    }

    componentDidUpdate(prevProps, prevState) {
        const { searchOpen } = this.state;

        if (searchOpen && searchOpen !== prevState.searchOpen && this.searchInputRef) {
            this.searchInputRef.focus();
        }
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleOutsideClick);
    }

    setSearchWrapperRef = (node) => {
        this.searchWrapperRef = node;
    };

    setSearchInputRef = (node) => {
        this.searchInputRef = node;
    };

    handleOutsideClick = (event) => {
        if (this.searchWrapperRef && !this.searchWrapperRef.contains(event.target)) {
          console.log('handleOutsideClick')
          console.log('this.searchWrapperRef',this.searchWrapperRef)
          console.log('event.target',event.target);

          this.props.MobileMenuActions.mobileMenuClose();
        }
    };

    handleOpenSearch = () => {
        this.props.MobileMenuActions.mobileMenuOpen();
    };

    handleCloseSearch = () => {
      console.log('handleCloseSearch')
        this.props.MobileMenuActions.mobileMenuClose();
    };

    handleSearchKeyDown = (event) => {
      console.log('handleSearchKeyDown')
        if (event.which === 27) {
            this.props.MobileMenuActions.mobileMenuClose();
        }
    };

    render() {
        const { MobileMenuActions, wishlist, cart } = this.props;
        console.log(MobileMenuActions);
        console.log(wishlist);
        console.log(cart);
        const cartJS = cart.toJS();
        const { searchOpen } = this.state;
        const searchClasses = classNames('mobile-header__search', {
            'mobile-header__search--opened': searchOpen,
        });

        return (
            <div className="mobile-header">
                <div className="mobile-header__panel">
                    <div className="container">
                        <div className="mobile-header__body">
                            {/* menu */}
                            <button type="button" className="mobile-header__menu-button" onClick={MobileMenuActions.mobileMenuOpen}>
                                <Menu18x14Svg />
                            </button>

                            {/* 나눠먹자 로고 */}
                            <Link to="/" className="mobile-header__logo"><LogoSmallSvg /></Link>


                            {/* 설치 아이콘 */}
                            <div className={searchClasses} ref={this.setSearchWrapperRef}>
                                <form className="mobile-header__search-form" action="">
                                    <input
                                        className="mobile-header__search-input"
                                        name="search"
                                        placeholder="할인된 음식 찾자"
                                        aria-label="Site search"
                                        type="text"
                                        autoComplete="off"
                                        onKeyDown={this.handleSearchKeyDown}
                                        ref={this.setSearchInputRef}
                                    />
                                    <button type="submit" className="mobile-header__search-button mobile-header__search-button--submit">
                                        <Search20Svg />
                                    </button>
                                    <button
                                        type="button"
                                        className="mobile-header__search-button mobile-header__search-button--close"
                                        onClick={this.handleCloseSearch}
                                    >
                                        <Cross20Svg />
                                    </button>
                                    <div className="mobile-header__search-body" />
                                </form>
                            </div>


                            {/* 설치 아이콘 */}
                            <div className="mobile-header__indicators">
                                <Indicator
                                    className="indicator--mobile indicator--mobile-search d-sm-none"
                                    onClick={this.handleOpenSearch}
                                    icon={<Search20Svg />}
                                />
                                <Indicator
                                    className="indicator--mobile d-sm-flex d-none"
                                    url="/shop/wishlist"
                                    value={wishlist.length}
                                    icon={<Heart20Svg />}
                                />
                                <Indicator
                                    className="indicator--mobile"
                                    url="/shop/cart"
                                    value={cartJS.quantity}
                                    icon={<Cart20Svg />}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    cart: state.cart,
    wishlist: state.wishlist,
});

export default connect(
    mapStateToProps,
    (dispatch) => ({
      MobileMenuActions : bindActionCreators(mobileMenuActions, dispatch),
    })
)(MobileHeader);
