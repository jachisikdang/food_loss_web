// react
import React from 'react';

import { paymentService } from '../../services';

class KakaoPay extends React.Component {
    constructor(props) {
        super(props);
    }

    // 결제 요청 함수
    requestPay = () => {
      const { orderId, itemname, amount,history } = this.props;
      console.log(orderId);
      paymentService.kakaoPayComplete(orderId, itemname, amount, history)

    }

    render() {

      return (
        <button type="submit" className="btn btn-primary btn-xl btn-block" onClick={this.requestPay}>결제하기</button>
      );
    }
  }

  export default KakaoPay;
