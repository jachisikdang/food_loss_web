// application
import enMessages from './messages/en';
import ruMessages from './messages/ru';
import krMessages from './messages/kr';

export default {
    kr: krMessages,
    en: enMessages,
    ru: ruMessages
};
