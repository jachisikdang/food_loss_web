import { createAction, handleActions } from 'redux-actions'
import { Map } from 'immutable'

// Actions
const CURRENCY_CHANGE = 'currency/CURRENCY_CHANGE';

// Actions Creators
export const currencyChange = createAction(CURRENCY_CHANGE); //  { form, name, value }

const initialState = Map({
    code: 'USD',
    symbol: '$',
    name: 'US Dollar',
});


export default handleActions({
  [CURRENCY_CHANGE] : (state,action) => {
    return state.set('code',action.currency.code)
              .set('symbol',action.currency.symbol)
              .set('name',action.currency.name)
  }
},initialState);
