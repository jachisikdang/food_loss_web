export default {
    name: '나눠먹자',
    fullName: 'Stroyka - Tools Store React eCommerce Template',
    url: 'https://themeforest.net/item/stroyka-tools-store-react-ecommerce-template/23909258',
    author: {
        name: 'Kos',
        profile_url: 'https://themeforest.net/user/kos9',
    },
    contacts: {
        address: '서울특별시 마포구 월드컵북로 481',
        email: 'jachisikdang@gmail.com',
        phone: '010-5159-3850',
    },
};
