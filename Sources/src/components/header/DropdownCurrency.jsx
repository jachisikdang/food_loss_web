// react
import React from 'react';

// third-party
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

// application
import Dropdown from './Dropdown';
import * as currencyActions from '../../redux/modules/currency';

// data stubs
import currencies from '../../data/shopCurrencies';


function DropdownCurrency(props) {
    const { currency, CurrencyActions } = this.props;

    const title = (
        <React.Fragment>
            <FormattedMessage id="topbar.currency" defaultMessage="Currency" />
            {': '}
            <span className="topbar__item-value">{currency.get('code')}</span>
        </React.Fragment>
    );

    return (
        <Dropdown
            title={title}
            items={currencies}
            onClick={(item) => CurrencyActions.changeCurrency(item.currency)}
        />
    );
}

export default connect(
  (state) => ({
    currency : state.currency,
    locale : state.locale
  }),
  (dispatch) => ({
    CurrencyActions : bindActionCreators(currencyActions, dispatch)
  })
)(DropdownCurrency);
