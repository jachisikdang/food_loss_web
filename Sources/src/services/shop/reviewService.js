import axios from 'axios';
import { config } from '../../config/Constants'

export function getReviews(itemsId) {
    return axios.get(`${config.API_URL}/v1/items/${itemsId}/comments`);
}
