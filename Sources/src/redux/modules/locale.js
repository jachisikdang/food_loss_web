import { createAction, handleActions } from 'redux-actions'

// Actions
const LOCALE_CHANGE = 'locale/LOCALE_CHANGE';

// Actions Creators
export const localeChange = createAction(LOCALE_CHANGE); //  { form, name, value }

const initialState = 'en';

export default handleActions({
  [LOCALE_CHANGE] : (state,action) => {
    return action.locale;
  }
},initialState);
