import { authenticationService } from '../services';

// JWT 인증을 위해 Header 생성을 도와주는 함수
export function authHeader() {
    // return authorization header with jwt token

    // TODO jwt react 사이트보고 이렇게 하도록 수정
    const currentUser = authenticationService.currentUserValue;

    if (currentUser && currentUser.token) {
        return {
          Authorization: `Bearer ${currentUser.token}`,
          'Content-Type': 'application/json'
       };
    } else {
        return {};
    }
}
