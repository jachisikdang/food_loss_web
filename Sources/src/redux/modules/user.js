
import { createAction, handleActions } from 'redux-actions'
import { pender } from 'redux-pender';
import { Map } from 'immutable'

import { authenticationService } from '../../services';


// Actions
const SET_LOGGED_INFO = 'user/SET_LOGGED_INFO';  // 로그인 정보 설정
const LOGOUT = 'user/LOGOUT';  // 로그아웃

// Action Creators
export const setLoggedInfo = createAction(SET_LOGGED_INFO);
export const logout = createAction(LOGOUT, authenticationService.logout);

const initialState = Map({
    loggedInfo: Map({ // 현재 로그인 중인 유저의 정보
      token: null,
      name: null,
      logged: false
    })
});

export default handleActions({
    [SET_LOGGED_INFO] : (state,action) => {
      // state.set('logged',true)
      // return state.set('loggedInfo', Map(action.payload))
      console.log('SET_LOGGED_INFO');
      console.log(state);
      console.log(action);
      return state.set('loggedInfo', Map(action.payload).set('logged',true).set('name',action.payload.name))
    },
    [LOGOUT] : (state,sction) => state.set('loggedInfo',initialState.get('loggedInfo'))

}, initialState);
