// react
import React, {Component} from 'react';

// third-party
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

// application
import Dropdown from './Dropdown';
import DropdownCurrency from './DropdownCurrency';
import DropdownLanguage from './DropdownLanguage';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../redux/modules/user';
import storage from '../../helpers/storage';
import { Map } from 'immutable'

import cookie from 'react-cookies'

// api service
import { memberService } from '../../services';

class Topbar extends Component {

  // 로그아웃 함수
  handleLogout = async () => {
    const { UserActions } = this.props;
    try {
      await UserActions.logout();
    } catch(e){
      console.log(e);
    }
    cookie.remove('login', { path: '/' });
    storage.remove('loggedInfo');

    window.location.href ='/';  // 홈페이지로 새로고침
  }

  render() {
    let user = this.props.user.toJS();

    const links = [
        { title: <FormattedMessage id="topbar.aboutUs" />, url: '/site/about-us' },
        { title: <FormattedMessage id="topbar.contacts"/>, url: '/seller' },
        // { title: <FormattedMessage id="topbar.storeLocation" defaultMessage="Store Location" />, url: '' },
        // { title: <FormattedMessage id="topbar.trackOrder" defaultMessage="Track Order" />, url: '/shop/track-order' },
        // { title: <FormattedMessage id="topbar.blog"/>, url: '/blog/category-classic' },
    ];

    const accountLinks = [
        // { title: 'Dashboard', url: '/account/dashboard' },
        { title: '내 정보', url: '/account/dashboard' },
        { title: '주문내역', url: '/account/orders' },
        { title: '취소/반품', url: '/account/orders' },
        { title: '찜 리스트', url: '/account/orders' },
        { title: '주소 등록', url: '/account/addresses' },
        { title: '로그아웃', url: '/account/login' },
    ];

    const linksList = links.map((item, index) => (
        <div key={index} className="topbar__item topbar__item--link">
            <Link className="topbar-link" to={item.url}>{item.title}</Link>
        </div>
    ));

    return (
        <div className="site-header__topbar topbar">
            <div className="topbar__container container">
                <div className="topbar__row">
                    {linksList}
                    <div className="topbar__spring" />

                    {
                      cookie.load('login') ?
                      (
                        <div className="topbar__item topbar__item--link">
                          <div className="topbar__item">
                              <Dropdown
                                  title={<FormattedMessage id="topbar.myAccount" defaultMessage="My Account" />}
                                  items={accountLinks}
                              />
                          </div>
                          <div className="topbar__item topbar__item--link">
                            <Link className="topbar-link" to="/account/orders">{user.loggedInfo.name} 님</Link>
                          </div>
                          <div className="topbar__item topbar__item--link" onClick={this.handleLogout}>로그아웃</div>
                        </div>
                      ) :
                      (
                        <div className="topbar__item topbar__item--link">
                          <div className="topbar__item topbar__item--link">
                              <Link className="topbar-link" to="/login">로그인</Link>
                          </div>

                          <div className="topbar__item topbar__item--link">
                              <Link className="topbar-link" to="/register">회원가입</Link>
                          </div>
                        </div>
                      )
                    }


                    <div className="topbar__item topbar__item--link">
                        <Link className="topbar-link" to="/site/contact-us">고객센터</Link>
                    </div>
                    {/* 너무너무 잘돼서 국제화가 필요해지면 그때 추가
                      <div className="topbar__item">
                          <DropdownCurrency />
                      </div>
                      <div className="topbar__item">
                          <DropdownLanguage />
                      </div>
                      */ }

                </div>
            </div>
        </div>
    );
  }
}

export default connect(
    (state) => ({
        user: state.user
    }),
    (dispatch) => ({
        UserActions: bindActionCreators(userActions, dispatch)
    })
)(Topbar);
