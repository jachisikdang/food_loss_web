import { authHeader } from '../helpers'
import storage from '../helpers/storage';
import { config } from '../config/Constants'

export const paymentService = {
    kakaoPayComplete
};

// 주문 버튼 클릭시 주문 생성 요청
export function kakaoPayComplete(orderId, itemname, amount, history) {

    console.log('orderId',orderId);
    console.log('itemname',itemname);
    console.log('history',history);
    /* 1. 가맹점 식별하기 */
    const { IMP } = window;

    /* 2. 결제 데이터 정의하기 */
    const data = {
      pg: "kakao",
      pay_method: "card",
      merchant_uid: `mid_${new Date().getTime()}`,
      name: "testitem",
      amount: amount
    };

    /* 4. 결제 창 호출하기 */
    IMP.request_pay(data,callback);


    /* 3. 콜백 함수 정의하기 */
    function callback(rsp){
      if (rsp.success) {
        // 결제 성공 시 로직,
        const headers = authHeader();

        const imp_uid = rsp.imp_uid;
        const merchant_uid = rsp.merchant_uid;
        const order_id = orderId;

        const requestOptions ={
          method: 'POST',
          body: JSON.stringify({ imp_uid, merchant_uid, order_id, amount }),
          headers: headers
        };

        console.log(requestOptions);

        return fetch(`${config.API_URL}/kakaopay/payments/complete`,requestOptions)
                .then(response => response.json())
                .then(data => {
                    const result = data.data;
                    switch(result.status) {
                      case "vbankIssued":
                        // 가상계좌 발급 시 로직
                        break;
                      case "success":
                        // 결제 성공 시 로직
                        console.log('success');
                        history.push(`/order/done?orderId=${result.order_id}`);
                        break;

                    }
                });
      } else {
        // 결제 실패 시 로직,
        console.log(rsp);
        alert("결제에 실패하였습니다. 에러 내용: " +  rsp.error_msg);
      }
    }
}
