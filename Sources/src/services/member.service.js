
import { authHeader } from '../helpers'
import { config } from '../config/Constants'

export const memberService = {
  getActiveUser
}

export function getActiveUser(){
    const requestOptions ={
      method : 'GET',
      headers: authHeader()
    }

    return fetch(`${config.API_URL}/v1/members/active`,requestOptions)
        .then(handleResponse => handleResponse.json())
        .then(response => response.data);
}
