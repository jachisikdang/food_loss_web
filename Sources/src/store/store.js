// third-party
import { createStore, applyMiddleware, compose } from 'redux';
import penderMiddleware from 'redux-pender';
import thunk from 'redux-thunk';

// reducer
import rootReducer from './rootReducer';


function load() {
    let state;

    try {
        state = localStorage.getItem('state');

        if (typeof state === 'string') {
            state = JSON.parse(state);
        }
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
    }

    return state || undefined;
}

const configureStore = (initialState) => {
  const store = createStore(rootReducer, initialState, compose(
      applyMiddleware(thunk,penderMiddleware())
  ));

  store.subscribe(() => save());


  function save() {
      try {
          localStorage.setItem('state', JSON.stringify(store.getState()));
      } catch (error) {
          // eslint-disable-next-line no-console
          console.error(error);
      }
  }

  return store;
}



export default configureStore;
