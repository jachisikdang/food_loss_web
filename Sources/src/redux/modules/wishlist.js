
import { createAction, handleActions } from 'redux-actions'
import { pender } from 'redux-pender';
import { Map, List } from 'immutable'

// Actions
const WISHLIST_ADD_ITEM = 'wishlist/WISHLIST_ADD_ITEM';
const WISHLIST_REMOVE_ITEM = 'wishlist/WISHLIST_REMOVE_ITEM';

// Actions Creators
export const wishlistAddItem = createAction(WISHLIST_ADD_ITEM); //  { form, name, value }
export const wishlistRemoveItem = createAction(WISHLIST_REMOVE_ITEM); // form

function addItem(state, product) {
    const itemIndex = state.findIndex(x => x.id === product.id);

    if (itemIndex === -1) {
      return state.push(Map({product}));
    }

    return state;
}


const initialState = List([]);

export default handleActions({
  [WISHLIST_ADD_ITEM] : (state,action) => {
    return addItem(state, action.product);
  },
  [WISHLIST_REMOVE_ITEM] : (state,action) => {
    return state.filter(x => x.id !== action.productId);
  }
},initialState);
