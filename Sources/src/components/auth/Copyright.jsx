import React from 'react';
import styled from 'styled-components';
import oc from 'open-color';

const Aligner = styled.div`
    margin-top: 1rem;
    text-align: center;
`;

const StyledLink = styled.div`
    color: ${oc.gray[7]};
    font-size: 0.7rem;
`

const Copyright = ({to, children}) => (
    <Aligner>
        <StyledLink>{children}</StyledLink>
    </Aligner>
);

export default Copyright;
