import { combineReducers } from 'redux';
import auth from './auth';
import cart from './cart';
import order from './order';
import compare from './compare';
import currency from './currency';
import locale from './locale';
import mobileMenu from './mobileMenu';
import quickview from './quickview';
import sidebar from './sidebar';
import user from './user';
import wishlist from './wishlist';

export default combineReducers({
    auth,
    cart,
    order,
    compare,
    currency,
    locale,
    mobileMenu,
    quickview,
    sidebar,
    user,
    wishlist
});
