// react
import React from 'react';

// third-party
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// application
import MobileLinks from './MobileLinks';
import { Cross20Svg } from '../../svg';
import * as currencyActions from '../../redux/modules/currency';
import * as localeActions from '../../redux/modules/locale';
import * as mobileMenuActions from '../../redux/modules/mobileMenu';
import * as userActions from '../../redux/modules/user';

// data stubs
import currencies from '../../data/shopCurrencies';
import mobileMenuLinks from '../../data/mobileMenu';

import storage from '../../helpers/storage';
import cookie from 'react-cookies'

function MobileMenu(props) {
    const {
        mobileMenuState,
        MobileMenuActions,
        LocaleActions,
        CurrencyActions,
        UserActions
    } = props;

    console.log('MobileMenu',props);
    const mobileMenuStateJs=mobileMenuState.toJS();

    const classes = classNames('mobilemenu', {
        'mobilemenu--open': mobileMenuStateJs.open,
    });

    // 메뉴 클릭시 동작들 정의
    const handleItemClick = (item) => {
        if (item.data) {
            if (item.data.type === 'language') {
                LocaleActions.changeLocale(item.data.locale);
                MobileMenuActions.closeMobileMenu();
            }
            else if (item.data.type === 'currency') {
                const currency = currencies.find((x) => x.currency.code === item.data.code);

                if (currency) {
                    CurrencyActions.changeCurrency(currency.currency);
                    MobileMenuActions.closeMobileMenu();
                }
            }
        }

        if(item.label === '로그아웃'){
            UserActions.logout();
            cookie.remove('login', { path: '/' });
            storage.remove('loggedInfo');
        }
    };

    return (
        <div className={classes}>
            {/* eslint-disable-next-line max-len */}
            {/* eslint-disable-next-line jsx-a11y/no-static-element-interactions,jsx-a11y/click-events-have-key-events */}
            <div className="mobilemenu__backdrop" onClick={MobileMenuActions.closeMobileMenu} />
            <div className="mobilemenu__body">
                <div className="mobilemenu__header">
                    <div className="mobilemenu__title">메뉴</div>
                    <button type="button" className="mobilemenu__close" onClick={MobileMenuActions.closeMobileMenu}>
                        <Cross20Svg />
                    </button>
                </div>
                <div className="mobilemenu__content">
                    <MobileLinks links={mobileMenuLinks} onItemClick={handleItemClick} />
                </div>
            </div>
        </div>
    );
}

export default connect(
  (state) => ({
    mobileMenuState : state.mobileMenu
  }),
  (dispatch) => ({
    MobileMenuActions : bindActionCreators(mobileMenuActions, dispatch),
    LocaleActions : bindActionCreators(localeActions, dispatch),
    CurrencyActions : bindActionCreators(currencyActions, dispatch),
    UserActions: bindActionCreators(userActions, dispatch)
  })
)(MobileMenu);
