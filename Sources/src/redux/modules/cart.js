
import { createAction, handleActions } from 'redux-actions'
import { pender } from 'redux-pender';
import { Map, List } from 'immutable'
import { toast } from 'react-toastify';

import { registerService, authenticationService }  from '../../services';

// Actions
const CART_ADD_ITEM = 'cart/CART_ADD_ITEM';
const CART_REMOVE_ITEM = 'cart/CART_REMOVE_ITEM';
const CART_UPDATE_QUANTITIES= 'cart/CART_UPDATE_QUANTITIES';

// Actions Creators
export const cartAddItem = createAction(CART_ADD_ITEM); //  { form, name, value }
export const cartRemoveItem = createAction(CART_REMOVE_ITEM); // form
export const cartUpdateQuantities = createAction(CART_UPDATE_QUANTITIES); // { form, message }


/**
 * @param {array} items
 * @param {object} product
 * @param {array} options
 * @return {number}
 */
function findItemIndex(items, product, options) {
    items = items.toJS();

    return items.findIndex((item) => {
        if (item.product.id !== product.id || item.options.length !== options.length) {
            return false;
        }

        for (let i = 0; i < options.length; i += 1) {
            const option = options[i];
            const itemOption = item.options.find(itemOption => (
                itemOption.optionId === option.optionId && itemOption.valueId === option.valueId
            ));

            if (!itemOption) {
                return false;
            }
        }

        return true;
    });
}


function calcSubtotal(items) {

    return items.reduce((subtotal, item) => subtotal + item.get('total'), 0);
}

function calcQuantity(items) {
    return items.reduce((quantity, item) => quantity + item.get('quantity'), 0);
}

function calcTotal(subtotal, extraLines) {
    return subtotal + extraLines.reduce((total, extraLine) => total + extraLine.get('price'), 0);
}


export function cartAddItems(product, options = [], quantity = 1) {
    // sending request to server, timeout is used as a stub
    return dispatch => (
        new Promise((resolve) => {
            setTimeout(() => {
                dispatch(cartAddItemSuccess(product, options, quantity));
                resolve();
            }, 2000);
        })
    );
}

export function cartAddItemSuccess(product, options = [], quantity = 1) {
    toast.success(`상품이 장바구니에 담겼습니다.`);

    return {
        type: CART_ADD_ITEM,
        product,
        options,
        quantity,
    };
}

function addItem(state, product, options, quantity) {
    console.log(product);
    const itemIndex = findItemIndex(state.get('items'), product, options);

    let newItems = state.get('items');
    let lastItemId = state.get('lastItemId');

    // 카트에 없는 물품인 경우, 새로운 item 배열에 생성
    if (itemIndex === -1) {
        lastItemId += 1;
        newItems = newItems.push(Map({
            id: lastItemId,
            product: product,
            options: product.options,
            price: product.price,
            total: product.price * product.quantity,
            quantity: product.quantity,
        }));


    }
    // 카트에 있는 물품인 경우, 해당 item의 정보를 update 해줌
    else {
        const item = state.get('items')[itemIndex];

        newItems = newItems.update(
          itemIndex,
          (item) => {
            item.set('quantity', item.get('quantity') + product.quantity)
                .set('total', (item.get('quantity') + product.quantity) * item.get('price'))

          }
        );
    }

    console.log('newItems',newItems);

    const subtotal = calcSubtotal(newItems);
    const total = calcTotal(subtotal, state.get('extraLines'));

console.log('subtotal:',subtotal);
console.log('total',total);
console.log(state);
    return state.set('lastItemId' , lastItemId)
                .set('subtotal', subtotal)
                .set('total', total)
                .set('items', newItems)
                .set('quantity', calcQuantity(newItems));
}



function removeItem(state, itemId) {
    const items = state.get('items');
    const newItems = items.filter(item => item.id !== itemId);

    const subtotal = calcSubtotal(newItems);
    const total = calcTotal(subtotal, state.get('extraLines'));

    return state.set('subtotal',subtotal)
                .set('total',total)
                .set('quantity',calcQuantity(newItems))
                .set('items', newItems);
}


function updateQuantities(state, quantities) {
    let needUpdate = false;

    const newItems = state.get('items').map((item) => {
        const quantity = quantities.find(x => x.itemId === item.id && x.value !== item.quantity);

        if (!quantity) {
            return item;
        }

        needUpdate = true;

        return state.set('quantity',quantity.value)
                    .set('total', quantity.value * item.price);
        return {
            ...item,
            quantity: quantity.value,
            total: quantity.value * item.price,
        };
    });

    if (needUpdate) {
        const subtotal = calcSubtotal(newItems);
        const total = calcTotal(subtotal, state.extraLines);

        return state.set('subtotal',subtotal)
                    .set('total',total)
                    .set('quantity',calcQuantity(newItems))
                    .set('items', newItems);
    }

    return state;
}

const initialState = Map({
  lastItemId: 0,
  quantity: 0,
  items: List([]),
  subtotal: 0,
  extraLines: List([ // shipping, taxes, fees, .etc
      Map({
          type: 'shipping',
          title: '배송비',
          price: 0,
      }),
      Map({
          type: 'tax',
          title: '세금',
          price: 0,
      }),
  ]),
  total: 0,
});

export default handleActions({
  [CART_ADD_ITEM] : (state,action) => {
    console.log('cart add item');
    console.log(state);
    console.log(action);
    return addItem(state, action.product, action.options, action.quantity);
  },
  [CART_REMOVE_ITEM] : (state,action) => {
    return removeItem(state, action.itemId)
  },
  [CART_UPDATE_QUANTITIES] : (state, action) => {
    return updateQuantities(state, action.quantities);
  }
},initialState);
