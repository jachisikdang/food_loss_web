import { authHeader } from '../helpers'
import storage from '../helpers/storage';
import { config } from '../config/Constants'

export const orderService = {
    createOrder,
    getOrderByMember
};

// 주문 버튼 클릭시 주문 생성 요청
export function createOrder(itemId, count) {

    // 회원 정보 가져오기
    // FIXME cookie로 가져오기
    const currentUser = storage.get('loggedInfo');

    // 로그인 안했으면 로그인 페이지로 전송
    if(currentUser == null)
      window.location.href="/login"

    const requestOptions ={
      method: 'POST',
      body: JSON.stringify({ itemId, count }),
      headers: authHeader()
    }

    return fetch(`${config.API_URL}/v1/orders`,requestOptions)
        .then(handleResponse => handleResponse.json());
}

// 고객의 주문내역 조회
export function getOrderByMember(orderStatus){
  const requestOptions = {
    method : 'GET',
    headers : authHeader()
  }

  return fetch(`${config.API_URL}/v1/orders/member?status=${orderStatus}`, requestOptions)
        .then(handleResponse => handleResponse.json());
}
