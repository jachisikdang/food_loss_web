// react
import React from 'react';

// third-party
import classNames from 'classnames';
import {
    Link,
    matchPath,
    Redirect,
    Switch,
    Route,
} from 'react-router-dom';

// application
import PageHeader from '../shared/PageHeader';

// router
import { PrivateRoute } from '../../routers/PrivateRoute'

// pages
import AccountPageAddresses from './AccountPageAddresses';
import AccountPageDashboard from './AccountPageDashboard';
import AccountPageOrders from './AccountPageOrders';
import AccountPagePassword from './AccountPagePassword';
import AccountPageProfile from './AccountPageProfile';


export default function AccountLayout(props) {
    const { match, location } = props;

    const breadcrumb = [
        { title: 'Home', url: '' },
        { title: '내정보', url: '' },
    ];

    // My 주문
    const orderLinks = [
        { title: '주문내역', url: 'orders' },
        { title: '취소/환불 내역', url: 'order-cancel' }
    ].map((link) => {
        const url = `${match.url}/${link.url}`;
        const isActive = matchPath(location.pathname, { path: url });
        const classes = classNames('account-nav__item', {
            'account-nav__item--active': isActive,
        });

        return (
            <li key={link.url} className={classes}>
                <Link to={url}>{link.title}</Link>
            </li>
        );
    });

    // My 정보
    const links = [
        { title: '개인정보확인', url: 'dashboard' },
        { title: '개인정보수정', url: 'profile' },
        { title: '주소 관라', url: 'addresses' },
        { title: '비밀번호 변경', url: 'password' },
    ].map((link) => {
        const url = `${match.url}/${link.url}`;
        const isActive = matchPath(location.pathname, { path: url });
        const classes = classNames('account-nav__item', {
            'account-nav__item--active': isActive,
        });

        return (
            <li key={link.url} className={classes}>
                <Link to={url}>{link.title}</Link>
            </li>
        );
    });

    return (
        <React.Fragment>
            <PageHeader header="My 나눠먹자" breadcrumb={breadcrumb} />

            <div className="block">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-lg-3 d-flex">
                            <div className="account-nav flex-grow-1">
                                <h4 className="account-nav__title">My 쇼핑</h4>
                                <ul>{orderLinks}</ul>

                                <h4 className="account-nav__title">My 정보</h4>
                                <ul>{links}</ul>
                            </div>
                        </div>

                        <div className="col-12 col-lg-9 mt-4 mt-lg-0">
                            <Switch>
                                <Redirect exact from={match.path} to={`${match.path}/dashboard`} />
                                <PrivateRoute exact path={`${match.path}/dashboard`} component={AccountPageDashboard} />
                                <PrivateRoute exact path={`${match.path}/profile`} component={AccountPageProfile} />
                                <PrivateRoute exact path={`${match.path}/orders`} component={AccountPageOrders} />
                                <PrivateRoute exact path={`${match.path}/addresses`} component={AccountPageAddresses} />
                                <PrivateRoute exact path={`${match.path}/password`} component={AccountPagePassword} />
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}
