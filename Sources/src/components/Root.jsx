// react
import React, { Component } from 'react';

// third-party
import PropTypes from 'prop-types';
import {
    BrowserRouter,
    Route,
    Redirect,
    Switch,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';

// application
import messages from '../i18n';

// pages
import Layout from './Layout';
import HomePageOne from './home/HomePageOne';
import HomePageTwo from './home/HomePageTwo';

import {bindActionCreators} from 'redux';
import * as userActions from '../redux/modules/user';
import storage from '../helpers/storage';

import { Map } from 'immutable'
import Cookies from 'js-cookie';

class Root extends Component {

  initializeUserInfo = async () => {

    console.log('Root - initializeUserInfo');

    // check login cookie
    const loginToken = Cookies.get('login');
    if(!loginToken)
      return;

    const loggedInfo = storage.get('loggedInfo');

    // 로그인 정보가 없다면 여기서 멈춥니다.
    if(!loggedInfo)
      return;

    const { UserActions } = this.props;
    console.log(loggedInfo);
    UserActions.setLoggedInfo(loggedInfo);

    // TODO 로그인 만료 관련 부분
    // try {
    //     await UserActions.checkStatus();
    // } catch (e) {
    //     storage.remove('loggedInfo');
    //     window.location.href = '/login?expired';
    // }
  }

  initializeIamport = () => {

      const { IMP } = window;
      IMP.init('imp96216405');
  }

  componentDidMount() {
      setTimeout(() => {
          const preloader = document.querySelector('.site-preloader');

          preloader.addEventListener('transitionend', (event) => {
              if (event.propertyName === 'opacity') {
                  preloader.parentNode.removeChild(preloader);
              }
          });
          preloader.classList.add('site-preloader__fade');
      }, 500);

      this.initializeUserInfo();
      this.initializeIamport();
  }

  render() {
      const { locale } = this.props;
      return (
          <IntlProvider locale={locale} messages={messages[locale]}>
              <BrowserRouter basename={process.env.PUBLIC_URL}>
                  <Switch>
                      <Route
                          path="/home-two"
                          render={(props) => (
                              <Layout {...props} headerLayout="compact" homeComponent={HomePageTwo} />
                          )}
                      />
                      <Route
                          path="/"
                          render={(props) => (
                              <Layout {...props} headerLayout="default" homeComponent={HomePageTwo} />
                          )}
                      />
                      <Redirect to="/" />
                  </Switch>
              </BrowserRouter>
          </IntlProvider>
      );
  }
}

Root.propTypes = {
    /** current locale */
    locale: PropTypes.string,
};

const mapStateToProps = (state) => ({
    locale: state.locale
});

export default connect(
  (mapStateToProps),
  (dispatch) => ({
        UserActions: bindActionCreators(userActions, dispatch)
  }))(Root);
