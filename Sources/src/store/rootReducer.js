import { combineReducers } from 'redux';

// TODO redux-immutable로 바꿔야 한다. 하지만 현재 cartReducer들은
// immutable을 사용하지 않기 때문에 inputState.withMutations is not a function
// 과 같은 에러가 발생한다.
// import { combineReducers } from 'redux-immutable';

// reducers
import compareReducer from '../redux/modules/compare';
import currencyReducer from '../redux/modules/currency';
import localeReducer from '../redux/modules/locale';
import authReducer from '../redux/modules/auth';
import user from '../redux/modules/user';
import cartReducer from '../redux/modules/cart';
import orderReducer from '../redux/modules/order';
import wishlistReducer from '../redux/modules/wishlist';
import mobileMenuReducer from '../redux/modules/mobileMenu';
import sidebarReducer from '../redux/modules/sidebar';
import quickviewReducer from '../redux/modules/quickview';

import { penderReducer } from 'redux-pender';

export default combineReducers({
    cart: cartReducer,
    order: orderReducer,
    compare: compareReducer,
    currency: currencyReducer,
    locale: localeReducer,
    mobileMenu: mobileMenuReducer,
    quickview: quickviewReducer,
    sidebar: sidebarReducer,
    wishlist: wishlistReducer,
    auth: authReducer,  // 로그인, 회원가입
    user,
    pender: penderReducer
});
