// react
import React from 'react';

// application
import FooterContacts from './FooterContacts';
import FooterLinks from './FooterLinks';
import FooterNewsletter from './FooterNewsletter';
import { LogoSvg } from '../../svg';

// data stubs
import theme from '../../data/theme';


export default function Footer() {
    const informationLinks = [
        { title: '서비스 소개', url: '/site/about-us' },
        { title: '공지사항', url: '' },
        { title: '연락하기', url: '/site/contact-us' }
    ];

    const accountLinks = [
        { title: '이용약관', url: '/policies/terms' },
        { title: '개인정보 처리방침', url: '/policies/privacy' },
        { title: '입점 / 제휴문의', url: '' }
    ];

    return (
        <div className="site-footer">
            <div className="container">
                <div className="site-footer__widgets">
                    <div className="row">
                        <div className="col-12 col-md-6 col-lg-3">
                            <LogoSvg />
                        </div>
                        <div className="col-12 col-md-6 col-lg-3">
                            <FooterContacts />
                        </div>
                        <div className="col-6 col-md-3 col-lg-2">
                            <FooterLinks title="회사소개" items={informationLinks} />
                        </div>
                        <div className="col-6 col-md-3 col-lg-2">
                            <FooterLinks title="고객지원" items={accountLinks} />
                        </div>
                        {/*
                        <div className="col-12 col-md-6 col-lg-2">
                            <FooterNewsletter />
                        </div>
                        */}
                    </div>
                </div>
            </div>
        </div>
    );
}
