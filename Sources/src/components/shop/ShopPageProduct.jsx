// react
import React, {useState, useEffect} from 'react';
// third-party
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';
// application
import PageHeader from '../shared/PageHeader';
import Product from '../shared/Product';
import ProductTabs from './ProductTabs';
// blocks
import BlockProductsCarousel from '../blocks/BlockProductsCarousel';
// widgets
import WidgetCategories from '../widgets/WidgetCategories';
import WidgetProducts from '../widgets/WidgetProducts';

// data stubs
import categories from '../../data/shopWidgetCategories';
import products from '../../data/shopProducts';
import theme from '../../data/theme';

import axios from 'axios';
import * as service from '../../services/shop/items.service'

import storage from '../../helpers/storage';


function ShopPageProduct(props) {
    const {
        layout,
        sidebarPosition,
        match
    } = props;

    const [ product, setProduct ] = useState(null);

    useEffect(()=>{
      // 상품 조회
      async function fetchData(){
        service.getItems(match.params.productId)
          .then(response=> response.json())
          .then(json=>setProduct(json.data));
      }

      fetchData();
    },[]);

    const goOrderPage = (orderId) => {
      const state = storage.get('state');
        console.log('order',state);

      props.history.push({
        pathname:`/shop/checkout/${orderId}`,
        state: { order : state.order }
      });
    }

    // 상품 상세
    if (match.params.productId) {
        //product = products.find((x) => x.id === parseFloat(match.params.productId));
        console.log('product');
        console.log(product);
    } else {
        // TODO 언제 있는 일일까??
        product = products[products.length - 1];
    }

    const breadcrumb = [
        {title: '나눠먹자 홈', url: '/'},
        {title: '상품', url: ''},
    ];

    let content;

    if (layout === 'sidebar') {
        const sidebar = (
            <div className="shop-layout__sidebar">
                <div className="block block-sidebar">
                    <div className="block-sidebar__item">
                        <WidgetCategories categories={categories} location="shop"/>
                    </div>
                    <div className="block-sidebar__item d-none d-lg-block">
                        <WidgetProducts title="Latest Products" products={products.slice(0, 5)}/>
                    </div>
                </div>
            </div>
        );

        content = (
            <div className="container">
                <div className={`shop-layout shop-layout--sidebar--${sidebarPosition}`}>
                    {sidebarPosition === 'start' && sidebar}
                    <div className=" shop-layout__content">
                        { product != null &&
                          <div className=" block">
                              <Product product={product} layout={layout}/>
                              { product != null &&
                                <ProductTabs withSidebar productId={product.idx}/>
                              }
                          </div>
                        }
                        <BlockProductsCarousel title="인근 지역 상품" layout="grid-4-sm" products={products}
                                               withSidebar/>
                    </div>
                    {sidebarPosition === 'end' && sidebar}
                </div>
            </div>
        );
    } else {
        content = (
            <React.Fragment>
                <div className="block">
                    <div className="container">

                        { product != null &&
                          <Product product={product} layout={layout} goOrderPage={goOrderPage}/>
                        }
                        <ProductTabs/>
                    </div>
                </div>

                <BlockProductsCarousel title="인근 지역 상품" layout="grid-5" products={products}/>
            </React.Fragment>
        );
    }

    return (
        <React.Fragment>
            <Helmet>
                <title>{ product ? `${product.name} — ${theme.name}`:''}</title>
            </Helmet>

            <PageHeader breadcrumb={breadcrumb}/>

            {content}
        </React.Fragment>
    );
}

ShopPageProduct.propTypes = {
    /** one of ['standard', 'sidebar', 'columnar', 'quickview'] (default: 'standard') */
    layout: PropTypes.oneOf(['standard', 'sidebar', 'columnar', 'quickview']),
    /**
     * sidebar position (default: 'start')
     * one of ['start', 'end']
     * for LTR scripts "start" is "left" and "end" is "right"
     */
    sidebarPosition: PropTypes.oneOf(['start', 'end']),
};

ShopPageProduct.defaultProps = {
    layout: 'standard',
    sidebarPosition: 'start',
};

export default ShopPageProduct;
