
import { createAction, handleActions } from 'redux-actions'
import { pender } from 'redux-pender';
import { Map } from 'immutable'

import { registerService, authenticationService }  from '../../services';

// Actions
const CHANGE_INPUT = 'auth/CHANGE_INPUT';
const INITIALIZE_FORM = 'auth/INITIALIZE_FORM';
const SET_ERROR= 'auth/SET_ERROR';
const CHECK_USERNAME_EXISTS= 'auth/CHECK_USERNAME_EXISTS'; // 아이디(이메일) 중복 확인
const REGISTER = 'auth/REGISTER';  // 회원가입
const LOGIN = 'auth/LOGIN';  // 로그인

// Actions Creators
export const changeInput = createAction(CHANGE_INPUT); //  { form, name, value }
export const initializeForm = createAction(INITIALIZE_FORM); // form
export const setError = createAction(SET_ERROR); // { form, message }
export const checkUsernameExists = createAction(CHECK_USERNAME_EXISTS, registerService.checkUsernameExists);

export const login = createAction(LOGIN, authenticationService.login);
export const register = createAction(REGISTER, registerService.register); // { username, password, name, phoneNumber }

// TODO redux ducks 구조라는데 처음 들어봄 공부 필요
const initialState = Map({
    register: Map({
        form: Map({
          username: '',
          password: '',
          passwordConfirm: '',
          name: '',
          phoneNumber: ''
        }),
        exists: Map({
          username: false,
          password: false
        }),
        error: null
    }),
    login: Map({
        form: Map({
            username: '',
            password: '',
            rememberMe: false
        }),
        error: null
    }),
    result: Map({
      token:'',
      name:''
    })
});

export default handleActions({
    [CHANGE_INPUT]: (state, action) => {
        console.log('change input');
        const { form, name, value } = action.payload;
        return state.setIn([form, 'form', name], value);
    },
    [INITIALIZE_FORM]: (state, action) => {
        const initialForm = initialState.get(action.payload);
        return state.set(action.payload, initialForm);
    },
    [SET_ERROR]: (state, action) => {
        const { form, message } = action.payload;
        return state.setIn([form,'error'],message);
    },
    ...pender({
      type: CHECK_USERNAME_EXISTS,
      onSuccess: (state, action) => {
        console.log('CHECK_USERNAME_EXISTS');
        const result = state.set(['register','exists','username'],action.payload.data.exists);
        return result;
      }
    }),
    ...pender({
      type: LOGIN,
      onSuccess: (state, action) => {
        console.log('LOGIN : ');
        const result = state.set('result', Map(action.payload));

        return result;
      }
    }),
    ...pender({
      type : REGISTER,
      onSuccess: (state, action) => {
        console.log('REGISTER : ' );
        console.log(state);
        console.log(action);
        return state.set('result', Map(action.payload))
      }
    })

}, initialState);
