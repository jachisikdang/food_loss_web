// react
import React from 'react';

// third-party
import { Link } from 'react-router-dom';


export default function BlockBanner() {
    return (
        <div className="block block-banner">
            <div className="container">
                <Link to="/" className="block-banner__body">
                    <div
                        className="block-banner__image block-banner__image--desktop"
                        style={{ backgroundImage: 'url("images/banners/banner-1.jpg")' }}
                    />
                    <div
                        className="block-banner__image block-banner__image--mobile"
                        style={{ backgroundImage: 'url("images/banners/banner-1-mobile.jpg")' }}
                    />
                    <div className="block-banner__title">
                        더 많은
                        <br className="block-banner__mobile-br" />
                        식품을 등록 중입니다
                    </div>
                    <div className="block-banner__text">
                        나눠먹자 사용자와 가게 사장님들이 더 만날 수 있도록 최선을 다하고 있습니다.
                    </div>
                    <div className="block-banner__button">
                        <span className="btn btn-sm btn-primary">쇼핑하기</span>
                    </div>
                </Link>
            </div>
        </div>
    );
}
