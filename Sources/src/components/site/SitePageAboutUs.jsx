// react
import React from 'react';

// third-party
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';

// application
import SlickWithPreventSwipeClick from '../shared/SlickWithPreventSwipeClick';

// data stubs
import theme from '../../data/theme';


const slickSettings = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 400,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            },
        },
        {
            breakpoint: 379,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            },
        },
    ],
};

function SitePageAboutUs() {
    return (
        <div className="block about-us">
            <Helmet>
                <title>{`나눠먹자는?`}</title>
            </Helmet>

            <div className="about-us__image" style={{ backgroundImage: 'url("images/aboutus2.jpg")' }} />
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-12 col-xl-10">
                        <div className="about-us__body">
                            <h1 className="about-us__title">나눠먹자는?</h1>
                            <div className="about-us__text typography">
                                <p>
                                    나눠먹자는 ~~~ 서비스고 ~~~ 서비스에여
                                </p>
                            </div>
                            <div className="about-us__team">
                                <h2 className="about-us__team-title">나눠먹자 팀원 소개</h2>
                                <div className="about-us__team-subtitle text-muted">
                                    저희와 함께 일하고 싶으신가요?
                                    <br />
                                    <Link to="/site/contact-us">여기로</Link>
                                    {' '}
                                    많은 지원 부탁드립니다
                                </div>
                                <div className="about-us__teammates teammates">
                                    <SlickWithPreventSwipeClick {...slickSettings}>
                                        <div className="teammates__item teammate">
                                            <div className="teammate__avatar">
                                                <img src="images/teammates/tedkim_profile.jpeg" alt="" />
                                            </div>
                                            <div className="teammate__name">김태현</div>
                                            <div className="teammate__position text-muted">대표 및 개발자</div>
                                        </div>
                                        <div className="teammates__item teammate">
                                            <div className="teammate__avatar">
                                                <img src="images/teammates/teammate-2.jpg" alt="" />
                                            </div>
                                            <div className="teammate__name">Katherine Miller</div>
                                            <div className="teammate__position text-muted">Marketing Officer</div>
                                        </div>
                                        <div className="teammates__item teammate">
                                            <div className="teammate__avatar">
                                                <img src="images/teammates/teammate-3.jpg" alt="" />
                                            </div>
                                            <div className="teammate__name">Anthony Harris</div>
                                            <div className="teammate__position text-muted">Finance Director</div>
                                        </div>
                                    </SlickWithPreventSwipeClick>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SitePageAboutUs;
