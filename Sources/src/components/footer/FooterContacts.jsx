// react
import React from 'react';

// data stubs
import theme from '../../data/theme';


export default function FooterContacts() {
    return (
        <div className="site-footer__widget footer-contacts">

            <div className="footer-contacts__text">
                나눠먹자(주) | 대표이사 : 김태현 <br/>
                사업자 등록번호 : 451-69-00321 <br/>
                <a href="http://www.ftc.go.kr/bizCommPop.do?wrkr_no=1208800767" target="_blank">사업자정보 확인하기</a>
            </div>

            <ul className="footer-contacts__contacts">
                <li>
                    <i className="footer-contacts__icon fas fa-globe-americas" />
                    {theme.contacts.address}
                </li>
                <li>
                    <i className="footer-contacts__icon far fa-envelope" />
                    {theme.contacts.email}
                </li>
                <li>
                    <i className="footer-contacts__icon fas fa-mobile-alt" />
                    {`${theme.contacts.phone}`}
                </li>
                <li>
                    <i className="footer-contacts__icon far fa-clock" />
                    월-토 10:00pm - 7:00pm
                </li>
            </ul>
        </div>
    );
}
