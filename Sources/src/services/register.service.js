
import { config } from '../config/Constants'
import storage from '../helpers/storage';
import { Map } from 'immutable'
// 3rd-library
import cookie from 'react-cookies'

export const registerService = {
    checkUsernameExists,
    register
};


export function checkUsernameExists(username){
  return fetch(`${config.API_URL}/v1/members?username=${username}`)
}

export function register(member) {
  console.log(member);
  const body = JSON.stringify(member);

  return fetch(`${config.API_URL}/v1/members`,
      {
        method: 'POST',
        body: body,
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(handleResponse => handleResponse.json())
      .then(response => {
          const loggedInfo = response.data
          console.log(response);

          storage.set('loggedInfo',Map(loggedInfo));
          // set cookie
          cookie.save("login", loggedInfo.token, {path: "/", maxAge: 60*60});

          return loggedInfo;
      });

}
