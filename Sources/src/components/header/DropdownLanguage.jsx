// react
import React from 'react';

// third-party
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { bindActionCreators } from 'redux';

// application
import Dropdown from './Dropdown';
import * as localeActions  from '../../redux/modules/locale';


function DropdownLanguage(props) {
    const { locale, LocaleActions } = props;

    const languages = [
        {
            title: '한국어',
            locale: 'kr',
            code: 'KR',
            icon: 'images/languages/language-1.png',
            icon_srcset: 'images/languages/language-1.png 1x, images/languages/language-1@2x.png 2x',
        },
        {
            title: 'English',
            locale: 'en',
            code: 'EN',
            icon: 'images/languages/language-1.png',
            icon_srcset: 'images/languages/language-1.png 1x, images/languages/language-1@2x.png 2x',
        },
        {
            title: 'Russian',
            locale: 'ru',
            code: 'RU',
            icon: 'images/languages/language-2.png',
            icon_srcset: 'images/languages/language-2.png 1x, images/languages/language-2@2x.png 2x',
        },
    ];

    const language = languages.find((x) => x.locale === locale);

    const title = (
        <React.Fragment>
            <FormattedMessage id="topbar.language" defaultMessage="Language" />
            {': '}
            <span className="topbar__item-value">{language.code}</span>
        </React.Fragment>
    );

    return (
        <Dropdown
            title={title}
            withIcons
            items={languages}
            onClick={(item) => LocaleActions.changeLocale(item.locale)}
        />
    );
}

export default connect(
  (state) => ({
    locale : state.locale
  }),
  (dispatch) => ({
    LocaleActions : bindActionCreators(localeActions, dispatch)
  })
)(DropdownLanguage);
