// react
import React from 'react';

// third-party
import { connect } from 'react-redux';
import { Modal } from 'reactstrap';
import { bindActionCreators } from 'redux';

// application
import Product from './Product';
import { Cross20Svg } from '../../svg';
import * as quickviewActions from '../../redux/modules/quickview';


function Quickview(props) {
    const { product, open, QuickviewActions } = props;

    let productView;

    if (product !== null) {
        productView = <Product product={product} layout="quickview" />;
    }

    return (
        <Modal isOpen={open} toggle={QuickviewActions.quickviewClose} centered size="xl">
            <div className="quickview">
                <button className="quickview__close" type="button" onClick={QuickviewActions.quickviewClose}>
                    <Cross20Svg />
                </button>

                {productView}
            </div>
        </Modal>
    );
}

export default connect(
    (state) => ({
        open: state.quickview.get('open'),
        product: state.quickview.get('product'),
    }),
    (dispatch) => ({
      QuickviewActions : bindActionCreators(quickviewActions, dispatch)
    })
)(Quickview);
