// react
import React, {useState, useEffect} from 'react';

// application
import Pagination from '../shared/Pagination';
import Rating from '../shared/Rating';

// api
import * as service from '../../services/shop/reviewService'

function ProductTabReviews(props) {
    const {
      productId
    } = props;

    const [ reviews, setReviews ] = useState(null);

    // component mount
    useEffect(() => {
      async function fetchData(){
        console.log('ProductTabReviews componet mount');
        console.log(props);
        const responses = await Promise.all([service.getReviews(productId)]);

        setReviews(responses[0].data.data);
      }

      fetchData();
    },[]);


    const reviewsList = reviews? reviews.comments.map((review, index) => (
        <li key={index} className="reviews-list__item">
            <div className="review">
                <div className="review__avatar"><img src={review.avatar} alt="" /></div>
                <div className=" review__content">
                    <div className=" review__author">{review.memberName}</div>
                    <div className=" review__rating">
                        <Rating value={review.rating} />
                    </div>
                    <div className=" review__text">{review.message}</div>
                    <div className=" review__date">{review.regDate}</div>
                </div>
            </div>
        </li>
    )) : '';

    return (
        <div className="reviews-view">
            <div className="reviews-view__list">
                <h3 className="reviews-view__header">상품평</h3>

                <div className="reviews-list">
                    <ol className="reviews-list__content">
                        {reviewsList}
                    </ol>
                    <div className="reviews-list__pagination">
                        <Pagination current={1} siblings={2} total={10} />
                    </div>
                </div>
            </div>

            <form className="reviews-view__form">
                <h3 className="reviews-view__header">상품평 쓰기</h3>
                <div className="row">
                    <div className="col-12 col-lg-9 col-xl-8">
                        <div className="form-row">
                            <div className="form-group col-md-4">
                                <label htmlFor="review-stars">Review Stars</label>
                                <select id="review-stars" className="form-control">
                                    <option>5 Stars Rating</option>
                                    <option>4 Stars Rating</option>
                                    <option>3 Stars Rating</option>
                                    <option>2 Stars Rating</option>
                                    <option>1 Stars Rating</option>
                                </select>
                            </div>
                            <div className="form-group col-md-4">
                                <label htmlFor="review-author">이름</label>
                                <input type="text" className="form-control" id="review-author" placeholder="Your Name" />
                            </div>
                            <div className="form-group col-md-4">
                                <label htmlFor="review-email">Email Address</label>
                                <input type="text" className="form-control" id="review-email" placeholder="Email Address" />
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="review-text">상품평</label>
                            <textarea className="form-control" id="review-text" rows="6" />
                        </div>
                        <div className="form-group mb-0">
                            <button type="submit" className="btn btn-primary btn-lg">Post Your Review</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default ProductTabReviews;
