// react
import React from 'react';

// third-party
import PropTypes from 'prop-types';

// application
import PageHeader from '../shared/PageHeader';
import Privacy from './Privacy';


export default function PrivacyPage(props) {
    const { layout, sidebarPosition } = props;

    let content = (
            <div className="row justify-content-center">
                <div className="col-md-12 col-lg-9 col-xl-8">
                    <Privacy layout={layout} />
                </div>
            </div>
        );

    const breadcrumbs = [
        { title: '홈', url: '/' },
        { title: '고객센터', url: '' },
        { title: '약관 및 정책', url: '' },
    ];

    return (
        <React.Fragment>
            <PageHeader breadcrumb={breadcrumbs} />

            <div className="container">{content}</div>
        </React.Fragment>
    );
}

PrivacyPage.propTypes = {
    /**
     * post layout
     * one of ['classic', 'full'] (default: 'classic')
     */
    layout: PropTypes.oneOf(['classic', 'full']),
    /**
     * sidebar position (default: 'start')
     * one of ['start', 'end']
     * for LTR scripts "start" is "left" and "end" is "right"
     */
    sidebarPosition: PropTypes.oneOf(['start', 'end']),
};

PrivacyPage.defaultProps = {
    layout: 'classic',
    sidebarPosition: 'start',
};
