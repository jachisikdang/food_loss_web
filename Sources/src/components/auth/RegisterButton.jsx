import React from 'react';
import styled from 'styled-components';
import oc from 'open-color';
import { shadow } from '../../helpers/styleUtils';

const Wrapper = styled.div`
    margin-top: 1rem;
    padding-top: 0.6rem;
    padding-bottom: 0.5rem;

    background: white;
    color: ${oc.red[6]};

    text-align: center;
    font-size: 1.25rem;
    font-weight: 500;

    cursor: pointer;
    user-select: none;
    transition: .2s all;

    border : 1px solid ${oc.gray[4]};

`;

const RegisterButton = ({children, onClick}) => (
    <Wrapper onClick={onClick}>
        {children}
    </Wrapper>
);

export default RegisterButton;
