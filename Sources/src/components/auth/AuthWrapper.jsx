import React from 'react';
import styled from 'styled-components';
import oc from 'open-color';
import { shadow } from '../../helpers/styleUtils';
import { Link } from 'react-router-dom';
import { LogoSvg } from '../../svg';

// 화면의 중앙에 위치시킨다
const Positioner = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

// 너비, 그림자 설정
const ShadowedBox = styled.div`
    width: 300px;

    @media only screen and (min-width: 768px) {
      width: 500px;
    }
    ${shadow(2)}
`;

// 로고
const LogoWrapper = styled.div`
    height: 4rem;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const Logo = styled(Link)`
    color: white;
    font-size: 2.4rem;
    letter-spacing: 5px;
    text-decoration: none;
`;

// children 이 들어가는 곳
const Contents = styled.div`
    background: white;
    padding: 2rem;
    height: auto;
`;

const AuthWrapper = ({children}) => (
    <Positioner>

        <LogoWrapper>
            <Link to="/"><LogoSvg /></Link>
        </LogoWrapper>

        <ShadowedBox>
            <Contents>
                {children}
            </Contents>
        </ShadowedBox>
    </Positioner>
);

export default AuthWrapper;
