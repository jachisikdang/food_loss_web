// react
import React from 'react';

// third-party
import { Helmet } from 'react-helmet';

// data stubs
import theme from '../../data/theme';


export default function AccountPagePassword() {
    return (
        <div className="card">
            <Helmet>
                <title>{`비밀번호 변경 — ${theme.name}`}</title>
            </Helmet>

            <div className="card-header">
                <h5>비밀번호 변경</h5>
            </div>
            <div className="card-divider" />
            <div className="card-body">
                <div className="row no-gutters">
                    <div className="col-12 col-lg-7 col-xl-6">
                        <div className="form-group">
                            <label htmlFor="password-current">현재 비밀번호</label>
                            <input
                                type="password"
                                className="form-control"
                                id="password-current"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password-new">신규 비밀번호</label>
                            <input
                                type="password"
                                className="form-control"
                                id="password-new"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password-confirm">비밀번호 재입력</label>
                            <input
                                type="password"
                                className="form-control"
                                id="password-confirm"
                            />
                        </div>

                        <div className="form-group mt-5 mb-0">
                            <button type="button" className="btn btn-primary">비밀번호 변경</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
