// react
import React from 'react';


export default function BlockMap() {
    return (
        <div className="block-map block">
            <div className="block-map__body">
                <iframe
                    title="Google Map"
                    src="https://maps.google.com/maps?q=%EC%83%81%EC%95%94%ED%95%9C%ED%99%94%EC%98%A4%EB%B2%A8%EB%A6%AC%EC%8A%A4%ED%81%AC2%EC%B0%A8&t=&z=15&ie=UTF8&iwloc=&output=embed"
                    frameBorder="0"
                    scrolling="no"
                    marginHeight="0"
                    marginWidth="0"
                />
            </div>
        </div>
    );
}
