// react
import React, { Component, useState, useEffect } from 'react';

// third-party
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

// application
import Pagination from '../shared/Pagination';

// data stubs
import theme from '../../data/theme';

import { orderService } from '../../services'


export default function AccountPageOrders(props){

    const [ orders, setOrders ] = useState([]);
    const [ page, setPage ] = useState(1);

    useEffect(()=>{
        async function fetchData(){
          orderService.getOrderByMember('PAY_FINISH')
            .then(json=> {
              console.log(json);
              setOrders(json.data)
            });
        }

        fetchData();
    },[]);

    const handlePageChange = (page) => {
      setPage(page);
    };


    const orderTableHeader =
      <tr>
          <th>주문상품</th>
          <th>결제날짜</th>
          <th>결제상태</th>
          <th>금액</th>
      </tr>
    ;

    return (
        <div className="card">
            <Helmet>
                <title>{`주문내역 — ${theme.name}`}</title>
            </Helmet>

            <div className="card-header">
                <h5>주문내역</h5>
            </div>
            <div className="card-divider" />
            <div className="card-table">
                <div className="table-responsive-sm">
                    <table>
                        <thead>
                            {orderTableHeader}
                        </thead>
                        {/* FIXME 현재는 order : orderItem = 1:1 구조로 짜져있음 */}
                        <tbody>
                            {
                              orders.map(order => (
                                  <tr key={order.id}>
                                      <td><Link to={`/shop/product/${order.orderItems[0].item.idx}`}>{order.orderItems[0].item.name}</Link></td>
                                      <td>{order.orderDate}</td>
                                      <td>{order.status}</td>
                                      <td>{order.totalPrice}</td>
                                  </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="card-divider" />
            <div className="card-footer">
                <Pagination current={page} total={3} onPageChange={handlePageChange} />
            </div>
        </div>
    );
}
