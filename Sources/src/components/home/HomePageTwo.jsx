// react
import React from 'react';

// third-party
import { Helmet } from 'react-helmet';

// blocks
import BlockBanner from '../blocks/BlockBanner';
import BlockBrands from '../blocks/BlockBrands';
import BlockCategories from '../blocks/BlockCategories';
import BlockFeatures from '../blocks/BlockFeatures';
import BlockPosts from '../blocks/BlockPosts';
import BlockProductColumns from '../blocks/BlockProductColumns';
import BlockProducts from '../blocks/BlockProducts';
import BlockSlideShow from '../blocks/BlockSlideShow';
import BlockTabbedProductsCarousel from '../blocks/BlockTabbedProductsCarousel';

// data stubs
import categories from '../../data/shopBlockCategories';
import posts from '../../data/blogPosts';
import products from '../../data/shopProducts';
import theme from '../../data/theme';


function HomePageTwo() {
    const columns = [
      {
          title: '평점 높은 상품',
          products: products.slice(0, 3),
      },
      {
          title: '할인 많이 하는 상품',
          products: products.slice(3, 6),
      },
      {
          title: '많이 팔린 회사',
          products: products.slice(6, 9),
      },
    ];

    return (
        <React.Fragment>
            <Helmet>
                <title>{`나눠먹자 — ${theme.name}`}</title>
            </Helmet>

            <BlockSlideShow />

            <BlockFeatures layout="boxed" />

            <BlockTabbedProductsCarousel title="우리 동네 상품" layout="grid-5" rows={2} />

            <BlockBanner />

            {/*
              <BlockProducts
                  title="인기상품"
                  layout="large-last"
                  featuredProduct={products[0]}
                  products={products.slice(1, 7)}
              />*/}


            {/*<BlockCategories title="인기 카테고리" layout="compact" categories={categories} />*/}

            <BlockTabbedProductsCarousel title="새로 등록된 상품" layout="grid-5" />

            {/*<BlockPosts title="Latest News" layout="grid-nl" posts={posts} />*/}

            {/*<BlockBrands />*/}

            {/* <BlockProductColumns columns={columns} /> */}
        </React.Fragment>
    );
}

export default HomePageTwo;
