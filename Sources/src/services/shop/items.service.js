
import { config } from '../../config/Constants'

export function getItems(id) {
    return fetch(`${config.API_URL}/v1/items/` + id);
}
