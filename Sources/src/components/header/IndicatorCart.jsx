// react
import React from 'react';

// third-party
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';

// application
import AsyncAction from '../shared/AsyncAction';
import Currency from '../shared/Currency';
import Indicator from './Indicator';
import { Cart20Svg, Cross10Svg } from '../../svg';
import * as cartActions from '../../redux/modules/cart';


function IndicatorCart(props) {
    const { cart, CartActions } = props;
    let dropdown;
    let totals;

    console.log(props);
    if (cart.get('extraLines').length > 0) {
        const extraLines = cart.get('extraLines').map((extraLine, index) => (
            <tr key={index}>
                <th>{extraLine.get('title')}</th>
                <td><Currency value={extraLine.get('price')} /></td>
            </tr>
        ));

        totals = (
            <React.Fragment>
                <tr>
                    <th>Subtotal</th>
                    <td><Currency value={cart.get('subtotal')} /></td>
                </tr>
                {extraLines}
            </React.Fragment>
        );
    }

    const items = cart.get('items').map((item) => {
        let options;
        let image;
        item = item.toJS();

        if (item.options) {
            options = (
                <ul className="dropcart__product-options">
                    {item.options.map((option, index) => (
                        <li key={index}>{`${option.optionTitle}: ${option.valueTitle}`}</li>
                    ))}
                </ul>
            );
        }

        if (item.product.images && item.product.images.length) {
            image = (
                <div className="dropcart__product-image">
                    <Link to={`/shop/product/${item.product.id}`}>
                        <img src={item.product.images[0]} alt="" />
                    </Link>
                </div>
            );
        }

        const removeButton = (
            <AsyncAction
                action={() => CartActions.cartRemoveItem(item.id)}
                render={({ run, loading }) => {
                    const classes = classNames('dropcart__product-remove btn btn-light btn-sm btn-svg-icon', {
                        'btn-loading': loading,
                    });

                    return (
                        <button type="button" onClick={run} className={classes}>
                            <Cross10Svg />
                        </button>
                    );
                }}
            />
        );

        return (
            <div key={item.id} className="dropcart__product">
                {image}
                <div className="dropcart__product-info">
                    <div className="dropcart__product-name">
                        <Link to={`/shop/product/${item.product.id}`}>{item.product.name}</Link>
                    </div>
                    {options}
                    <div className="dropcart__product-meta">
                        <span className="dropcart__product-quantity">{item.quantity}</span>
                        {' x '}
                        <span className="dropcart__product-price"><Currency value={item.price} /></span>
                    </div>
                </div>
                {removeButton}
            </div>
        );
    });

    if (cart.quantity) {
        dropdown = (
            <div className="dropcart">
                <div className="dropcart__products-list">
                    {items}
                </div>

                <div className="dropcart__totals">
                    <table>
                        <tbody>
                            {totals}
                            <tr>
                                <th>Total</th>
                                <td><Currency value={cart.total} /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div className="dropcart__buttons">
                    <Link className="btn btn-secondary" to="/shop/cart">장바구니 보기</Link>
                    <Link className="btn btn-primary" to="/shop/checkout">구매하기</Link>
                </div>
            </div>
        );
    } else {
        dropdown = (
            <div className="dropcart">
                <div className="dropcart__empty">
                    장바구니가 비어있습니다!
                </div>
            </div>
        );
    }

    return (
        <Indicator url="/shop/cart" dropdown={dropdown} value={cart.quantity} icon={<Cart20Svg />} />
    );
}

export default connect(
  (state) => ({
    cart : state.cart
  }),
  (dispatch) => ({
    CartActions : bindActionCreators(cartActions, dispatch)
  })
)(IndicatorCart);
