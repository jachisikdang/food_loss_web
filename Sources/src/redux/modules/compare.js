
import { createAction, handleActions } from 'redux-actions'
import { pender } from 'redux-pender';
import { Map,List } from 'immutable'


// Actions
const COMPARE_ADD_ITEM = 'compare/COMPARE_ADD_ITEM';
const COMPARE_REMOVE_ITEM = 'compare/COMPARE_REMOVE_ITEM';

// Actions Creators
export const compareAddItem = createAction(COMPARE_ADD_ITEM); //  { form, name, value }
export const compareRemoveItem = createAction(COMPARE_REMOVE_ITEM); // form


function addItem(state, product) {
    const itemIndex = state.findIndex(x => x.id === product.id);

    if (itemIndex === -1) {
      return state.push(product);
    }

    return state;
}


const initialState = List()

export default handleActions({
    [COMPARE_ADD_ITEM]: (state, action) => {
      return addItem(state, action.product);
    },
    [COMPARE_REMOVE_ITEM]: (state, action) => {
      return state.filter(x => x.id !== action.productId);
    }
}, initialState);
