// react
import React from 'react';

// third-party
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';

// application
import * as serviceWorker from './serviceWorker';
import Root from './components/Root';
import configureStore from './store';

// styles
import 'slick-carousel/slick/slick.css';
import 'react-toastify/dist/ReactToastify.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'react-input-range/lib/css/index.css';
import './scss/style.scss';

const store = configureStore();


ReactDOM.render((
    // eslint-disable-next-line react/jsx-filename-extension
    <CookiesProvider>
      <Provider store={store}>
        <Root />
      </Provider>
    </CookiesProvider>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
