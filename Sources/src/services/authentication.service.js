import { BehaviorSubject } from 'rxjs';

import { handleResponse } from '../helpers';
import { config } from '../config/Constants'
import storage from '../helpers/storage';
import { Map } from 'immutable'

import { authHeader } from '../helpers'
// const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

export const authenticationService = {
    login,
    logout,
    currentUser : storage.get('loggedInfo'),
    get currentUserValue() { return storage.get('loggedInfo') }
    // currentUser: currentUserSubject.asObservable(),
    // get currentUserValue () { return currentUserSubject.value }
};

function login({username, password, rememberMe}) {

    const body = JSON.stringify({ username, password, rememberMe });

    return fetch(`${config.API_URL}/v1/authenticate`,
        {
          method: 'POST',
          body: body,
          headers: {
            'Content-Type': 'application/json'
          }
        })
        .then(handleResponse => handleResponse.json())
        .then(response => {
            console.log(response);
            const loggedInfo = response.data
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            // localStorage.setItem('currentUser', JSON.stringify({ token : token, name }));
            storage.set('loggedInfo',Map(loggedInfo));
            // currentUserSubject.next(user);

            // FIXME 쿠키 서버에서 내려주는게 설정이 안돼서 임시로 클라이언트에서 설정함
            return loggedInfo;
        });
}

function logout() {
    // remove user from local storage to log user out

    const requestOptions ={
      headers: authHeader()
    }

    fetch(`${config.API_URL}/v1/logout`,requestOptions);

    storage.remove('loggedInfo');
    // currentUserSubject.next(null);
}
