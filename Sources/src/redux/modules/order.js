import { createAction, handleActions } from 'redux-actions'
import { pender } from 'redux-pender';
import { Map, List } from 'immutable'
import { toast } from 'react-toastify';

// Actions
const ORDER_ITEM = 'order/ORDER_ITEM';

// Actions Creators
export const orderItem = createAction(ORDER_ITEM); //  { form, name, value }

function addOrderItem(state, product, quantity){

  console.log('state',state);
  console.log('product',product);
  console.log('quantity',quantity);

  return state.set('quantity', quantity)
              .set('item', Map({
                id: product.idx,
                name: product.name,
                price: product.price,
                total: product.price * quantity
              }));
}

const initialState = Map({
  quantity: 0,
  item: Map({
      id: 0,
      name: '',
      price: 0,
      total: 0
  }),
  extraLines: List([ // shipping, taxes, fees, .etc
      Map({
          type: 'shipping',
          title: '배송비',
          price: 0,
      }),
      Map({
          type: 'tax',
          title: '세금',
          price: 0,
      })
  ])
});

export default handleActions({
  [ORDER_ITEM] : (state,action) => {
    console.log('order add item');
    console.log(state);
    console.log(action);
    return addOrderItem(state, action.payload.product, action.payload.quantity);
  }
},initialState);
