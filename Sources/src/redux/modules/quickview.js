import { createAction, handleActions } from 'redux-actions'
import { Map } from 'immutable'

// Actions
const QUICKVIEW_CLOSE = 'quickview/QUICKVIEW_CLOSE';
const QUICKVIEW_OPEN = 'quickview/QUICKVIEW_OPEN';

// Actions Creators
export const quickviewClose = createAction(QUICKVIEW_CLOSE); //  { form, name, value }
export const quickviewOpen = createAction(QUICKVIEW_OPEN); // form

const initialState = Map({
    open: false,
    product: null,
});

export default handleActions({
  [QUICKVIEW_OPEN]: (state,action) => {
    return state.set('open',true).
                set('product',Map(action.product));
  },
  [QUICKVIEW_CLOSE]: (state,action) => {
    return state.set('open',false);
  }
},initialState);
