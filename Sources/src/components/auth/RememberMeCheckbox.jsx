import React from 'react';
import styled from 'styled-components';
import oc from 'open-color';

// 두개가 함께 있을땐 상단 (그 사이) 에 여백을 준다
const Wrapper = styled.div`
    & + & {
        margin-top: 5rem;

        overflow: hidden;
    }
`;

const Label = styled.label`
    font-size: 1rem;
    color: ${oc.gray[6]};
    padding-left : 0.5rem;
    margin-bottom: 0.25rem;
`;

const Input = styled.input`
    border: 1px solid ${oc.gray[4]};
    outline: none;
    margin-top: 1rem;
    border-radius: 0px;
    line-height: 2.5rem;
    font-size: 1.1rem;
    padding-left: 0.5rem;
    padding-right: 0.5rem;
    ::placeholder {
    color: ${oc.gray[4]};

}
`;

// rest 쪽에는 onChange, type, name, value, placeholder 등의 input 에서 사용 하는 값들을 넣어줄수 있다.
const RememberMeCheckbox = ({label, ...rest}) => (
    <Wrapper>
        <Input {...rest}/>
        <Label for="remember-me">{label}</Label>
    </Wrapper>
);

export default RememberMeCheckbox;
