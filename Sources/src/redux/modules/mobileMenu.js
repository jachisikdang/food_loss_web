
import { createAction, handleActions } from 'redux-actions'
import { Map } from 'immutable'

// Actions
const MOBILE_MENU_CLOSE = 'mobile-menu/MOBILE_MENU_CLOSE';
const MOBILE_MENU_OPEN = 'mobile-menu/MOBILE_MENU_OPEN';

// Actions Creators
export const mobileMenuClose = createAction(MOBILE_MENU_CLOSE); //  { form, name, value }
export const mobileMenuOpen = createAction(MOBILE_MENU_OPEN); // form

const initialState = Map({
    open: false,
});

export default handleActions({
  [MOBILE_MENU_OPEN]: (state,action) => {
    return state.set('open',true);
  },
  [MOBILE_MENU_CLOSE]: (state,action) => {
    return state.set('open',false);
  }
},initialState);
