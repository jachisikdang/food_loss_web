import React, { Component } from 'react';
import { AuthContent, AuthWrapper, InputWithLabel, AuthButton, AuthError, RightAlignedLink, Copyright, RegisterButton, RememberMeCheckbox } from './';

// redux
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';

import * as authActions from '../../redux/modules/auth';
import * as userActions from '../../redux/modules/user';

import storage from '../../helpers/storage';
import { Map } from 'immutable'

// 3rd-library
import cookie from 'react-cookies'

class Login extends Component {

    componentWillUnmount() {
       const { AuthActions } = this.props;
       AuthActions.initializeForm('login')
    }

    setError = (message) => {
      const { AuthActions } = this.props;
      AuthActions.setError({
        form: 'login',
        message
      });
      return false;
    }

    handleChange = (e) => {
      const { AuthActions } = this.props;
      const { name, value } = e.target;

      AuthActions.changeInput({
          name,
          value,
          form: 'login'
      });
    }

    handleLogin = async () => {
      const { form, AuthActions, UserActions, history } = this.props;
      const { username, password, rememberMe } = form.toJS();

      try {
          const loggedInfo = await AuthActions.login({username, password, rememberMe});

          // FIXME 로그인 실패를 이렇게 판단하는거 말고 더 우아하게 response로 판단하자
          if(loggedInfo === undefined){
            console.log('login failure');
            this.setError('잘못된 계정정보입니다.');
          }
          else{
            console.log('handleLogin Success');
            console.log(loggedInfo);
            UserActions.setLoggedInfo(loggedInfo);

            // set cookie
            cookie.save("login", loggedInfo.token, {path: "/", maxAge: 60*60});
            // redirect to home
            history.push('/');
          }

      } catch (e) {
          console.log('error');
          console.log(e);
          this.setError('잘못된 계정정보입니다.');
      }
    }

    redirectRegister = () => {
      const { history } =this.props;
      history.push('/register');
    }

    render() {
      console.log(this.props);
      const { username, password, rememberMe } = this.props.form.toJS(); // form 에서 username 과 password 값을 읽어옴
      const { handleChange, handleLogin, redirectRegister } = this;
      const { error } = this.props;

      return (
          <AuthWrapper>
            <AuthContent >
              <InputWithLabel name="username" placeholder="아이디(이메일)" value={username} onChange={handleChange}/>
              <InputWithLabel name="password" placeholder="비밀번호" type="password" value={password} onChange={handleChange}/>

              <RememberMeCheckbox id="remember-me" name="rememberMe" label="자동로그인" type="checkbox" value={rememberMe} onClick={handleChange}/>
              {
                error && <AuthError>{error}</AuthError>
              }
              <AuthButton onClick={handleLogin}>로그인</AuthButton>
              <RegisterButton onClick={redirectRegister}>회원가입</RegisterButton>
              <RightAlignedLink to="/register">아이디/비밀번호 찾기</RightAlignedLink>
              <Copyright>©나눠먹자 Corp. All rights reserved.</Copyright>
            </AuthContent>
          </AuthWrapper>
      );
    }
}


export default connect(
  (state) => ({
    form: state.auth.getIn(['login', 'form']),
    error: state.auth.getIn(['login', 'error']),
    result: state.auth.get('result')
  }),
  (dispatch) => ({
    AuthActions: bindActionCreators(authActions, dispatch),
    UserActions: bindActionCreators(userActions, dispatch)
  })
)(Login);
