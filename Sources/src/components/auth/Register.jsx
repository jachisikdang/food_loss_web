import React, { Component } from 'react';
import Header from './Header';

import { AuthContent, AuthWrapper, InputWithLabel, AuthButton, AuthError } from './';

import {isEmail, isLength, isAlphanumeric, isMobilePhone} from 'validator';
import debounce from 'lodash/debounce'; // 특정 함수가 반복적으로 일어나면, 바로 실행하지 않고, 주어진 시간만큼 쉬어줘야 함수가 실행됩니다.

// redux 관련
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as authActions from '../../redux/modules/auth';
import * as userActions from '../../redux/modules/user';

import storage from '../../helpers/storage';
import { Map } from 'immutable'

class Register extends Component{

  // 로그인 -> 회원 가입 다녀오면 상태값이 남아있기 때문에 스토어 초기화
  componentWillUnmount() {
    const { AuthActions } = this.props;
    console.log('componentWillUnmount');
    AuthActions.initializeForm('register')
  }

  setError = (message) => {
    const { AuthActions } = this.props;
    AuthActions.setError({
        form: 'register',
        message
    });
  }

  validate = {
    username: (value) => {
        if(!isEmail(value)) {
            this.setError('잘못된 이메일 형식 입니다.');
            return false;
        }
        return true;
    },
    password: (value) => {
        if(!isLength(value, { min: 8 })) {
            this.setError('비밀번호를 8자 이상 입력하세요.');
            return false;
        }
        this.setError(null); // 이메일과 아이디는 에러 null 처리를 중복확인 부분에서 하게 됩니다
        return true;
    },
    passwordConfirm: (value) => {
        if(this.props.form.get('password') !== value) {
            this.setError('비밀번호가 일치하지 않습니다.');
            return false;
        }
        this.setError(null);
        return true;
    },
    name: (value) => {
      return true;
    },
    phoneNumber: (value) => {
      if(!isMobilePhone(value, 'ko-KR')){
        this.setError('핸드폰 번호 양식이 아닙니다.');
        return false;
      }
      this.setError(null);
      return true;
    }
  };

  // 회원 아이디(이메일)이 존재하는지 체크
  checkUsernameExists = debounce(async (username) => {
      const { AuthActions } = this.props;
      try {
          await AuthActions.checkUsernameExists(username);
          if(this.props.exists.get('username')) {
              this.setError('이미 존재하는 아이디입니다.');
          } else {
              this.setError(null);
          }
      } catch (e) {
          console.log(e);
      }
  }, 300);

  handleChange = (e) => {
    const { AuthActions } = this.props;
    const { name, value } = e.target;
    AuthActions.changeInput({
      name,
      value,
      form: 'register'
    });

    // 검증 작업 진행
    const validation = this.validate[name](value);

    // 비밀번호 검증이거나, 검증 실패하면 마침
    if(name.indexOf('password') > -1 || !validation)
      return;

    // 회원 존재 여부 판단
    if(name.indexOf('username') > -1 ){
      const check = this.checkUsernameExists;
      check(value);
    }
  }

  // 회원 가입 함수
  handleRegister = async () => {
    const { form, error, AuthActions, UserActions, history } = this.props;
    const { username, password, passwordConfirm, name, phoneNumber } = form.toJS();

    const { validate } = this;

    console.log(form.toJS());
    if(error) return;


    // 하나라도 실패하면 진행하지 않음
    if(!validate['username'](username)
        || !validate['password'](password)
        || !validate['passwordConfirm'](passwordConfirm)) {

        return;
    }

    // 회원 가입 API
    try {
      const loggedInfo = await AuthActions.register({
        username, password, name, phoneNumber
      });

      console.log(loggedInfo);
      UserActions.setLoggedInfo(loggedInfo);

      // redirect to home
      history.push('/');
    } catch(e){
      console.log(e);
      // 에러 처리하기
      if(e.response.status === 400 ) {
        return this.setError('이미 존재하는 아이디입니다.');
      }
      this.setError('알 수 없는 에러가 발생하였습니다.');
    }
  }

  render() {

    const { form,error } = this.props;
    const { username, password, passwordConfirm, name } = this.props.form.toJS();
    const { handleChange, handleRegister } = this;

    return (
      <AuthWrapper>
        <AuthContent title="회원가입">
          <InputWithLabel label="아이디" name="username" placeholder="아이디(이메일)" onChange={handleChange}/>
          <InputWithLabel label="비밀번호" name="password" placeholder="비밀번호" type="password" onChange={handleChange}/>
          <InputWithLabel label="비밀번호 확인" name="passwordConfirm"  placeholder="비밀번호 확인" type="password" onChange={handleChange}/>

          <InputWithLabel label="이름" name="name" placeholder="이름" onChange={handleChange}/>
          <InputWithLabel label="휴대폰 번호" name="phoneNumber" placeholder="휴대폰 번호" onChange={handleChange}/>

          {
            error && <AuthError>{error}</AuthError>
          }

          <AuthButton onClick={handleRegister} >회원가입</AuthButton>
        </AuthContent>
      </AuthWrapper>



    );
  }
}

export default connect(
  (state) => ({
      form: state.auth.getIn(['register', 'form']),
      error: state.auth.getIn(['register', 'error']),
      exists: state.auth.getIn(['register', 'exists']),
      result: state.auth.get('result')
  }),
  (dispatch) => ({
      AuthActions: bindActionCreators(authActions, dispatch),
      UserActions: bindActionCreators(userActions, dispatch)
  })
)(Register);
