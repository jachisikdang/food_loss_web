// react
import React from 'react';

// third-party
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// application
import BlogCommentsList from './BlogCommentsList';

// data stubs
import comments from '../../data/blogPostComments';
import posts from '../../data/blogPosts';


export default function BlogPost(props) {
    const { layout } = props;

    const postClasses = classNames('post__content typography', {
        'typography--expanded': layout === 'full',
    });

    const relatedPostsList = posts.slice(0, 2).map((relatedPost) => (
        <div key={relatedPost.id} className="related-posts__item post-card post-card--layout--related">
            <div className="post-card__image">
                <Link to="/">
                    <img src={relatedPost.image} alt="" />
                </Link>
            </div>
            <div className="post-card__info">
                <div className="post-card__name">
                    <Link to="/">{relatedPost.title}</Link>
                </div>
                <div className="post-card__date">{relatedPost.date}</div>
            </div>
        </div>
    ));

    return (
        <div className={`block post post--layout--${layout}`}>
            <div className={`post__header post-header post-header--layout--${layout}`}>
                <div className="post-header__categories">
                    <Link to="/">서비스 소개</Link>
                </div>
                <h1 className="post-header__title">나눠먹자 서비스는</h1>
            </div>

            <div className="post__featured">
                  <img src="images/posts/post-main.jpg" alt="" />
            </div>

            <div className={postClasses}>

                <blockquote>
                    <p>
                        나눠먹자는 사용자, 가게, 환경 모두가 행복해지는게 목표인 서비스입니다.
                    </p>
                    <p><cite>우리의 목표</cite></p>
                </blockquote>

                <p>
                    나눠먹자는 폐기 위기의 음식을 구하기 위한 플랫폼입니다. <br/><br/>
                    아직 맛있게 먹을 수 있지만, 폐점 시간이나 유통 기한 등의 이유로 버려질 음식을<br/><br/>
                    나눠먹자 사용자가 싼 가격에 음식을 구할 수 있습니다.<br/><br/>
                </p>

                <h3>손님도 해피, 가게도 해피</h3>
                <figure>
                    <Link to="/">
                        <img src="images/posts/post-featured2.jpg" alt="" />
                    </Link>
                    <figcaption>나눠먹자 사용자가 음식을 구매하면서 새로운 가게를 발견도 합니다.</figcaption>
                </figure>
                <p>
                    나눠먹자 서비스 사용자는 환경도 지키고 지갑도 지키면서 맛있는 식사를 구할 수 있습니다. <br/><br/>

                    어느 가게든 재고 및 매출에 맞추려고 노력을 통해 버려지는 음식을 최소화 하려 합니다. <br/><br/>

                    하지만, 날씨나 주변 행사의 영향으로 그 날 준비했던 음식을 버려야 하는 상황이 발생합니다. <br/><br/>

                    그럴 때 나눠먹자 서비스를 통해 식사를 게재하면, 나눠먹자 사용자가 나타나 <br/><br/>

                    버려지는 음식도 지켜주고, 가게의 손실도 지킬 수 있습니다.

                </p>
                <p>
                    <Link to="/"> 나눠먹자 가게 등록 신청하기</Link>
                </p>

                <hr />
                <h2>버려지는 못난이 음식들</h2>
                <figure>
                    <Link to="/">
                        <img src="images/posts/post-featured3.jpg" alt="" />
                    </Link>
                    <figcaption>모양이 망가져서 팔지 못하는 음식들을 소개해주는 서비스</figcaption>
                </figure>
                <p>
                    농어업 관계된 분들이 품질은 좋은데 모양이 망가져서 팔지 못하는 경우가 많습니다.<br/><br/>

                    모양보다는 합리적인 가격을 더 선호하는 나눠먹자 사용자들에게<br/><br/>

                    식품 정보를 공유하는 서비스입니다.<br/><br/>
                </p>
            </div>

            <div className="post__footer">
                <div className="post__tags-share-links">
                    <div className="post__tags tags">
                        <div className="tags__list">
                            <Link to="/">음식 손실</Link>
                            <Link to="/">환경 보호</Link>
                            <Link to="/">저렴한 구매</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

BlogPost.propTypes = {
    /**
     * post layout
     * one of ['classic', 'full'] (default: 'classic')
     */
    layout: PropTypes.oneOf(['classic', 'full']),
};

BlogPost.defaultProps = {
    layout: 'classic',
};
