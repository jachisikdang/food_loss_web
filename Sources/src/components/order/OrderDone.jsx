// react
import React from 'react';

// third-party
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link, Redirect } from 'react-router-dom';

// application
import Collapse from '../shared/Collapse';
import Currency from '../shared/Currency';
import PageHeader from '../shared/PageHeader';
import { Check9x7Svg } from '../../svg';

// data stubs
import payments from '../../data/shopPayments';
import theme from '../../data/theme';

export default function OrderDone(props){
      const breadcrumb = [
          { title: '상품선택', url: '' },
          { title: '주문/결제', url: '' },
          { title: '주문완료', url: '' }
      ];

      return (
          <React.Fragment>
              <Helmet>
                  <title>{`주문완료 — ${theme.name}`}</title>
              </Helmet>

              <PageHeader header="주문완료" breadcrumb={breadcrumb} />
              <div className="block">
                  <div className="container">
                      <div className="document">
                          <div className="document__header">
                              <h3 className="document__title">주문이 완료되었습니다. 감사합니다!</h3>
                              <div className="document__subtitle">환경과 고객을 생각하는 나눠먹자가 되도록 노력하겠습니다</div>
                          </div>
                          <div className="d-flex justify-content-center">
                            <Link to="/account/orders" className="btn btn-secondary btn-sm">주문 내역보기</Link>
                            <Link to="/" className="btn btn-primary btn-sm ml-2">쇼핑 계속하기</Link>
                          </div>
                      </div>
                  </div>

              </div>
          </React.Fragment>
      )
}
