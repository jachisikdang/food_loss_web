export default [
    {
        type: 'link',
        label: '홈',
        url: '/',
    },

    {
        type: 'link',
        label: '서비스 소개',
        url: '/site/about-us',
    },

    {
        type: 'link',
        label: '입점 신청',
        url: '/site/about-us',
    },

    {
        type: 'link',
        label: '로그인',
        url: '/login',
    },
    {
        type: 'link',
        label: '마이나눠먹자',
        url: '',
        children: [
            { type: 'link', label: '주문내역', url: '/shop/category-list' },
            { type: 'link', label: '취소/반품', url: '/shop/category-right-sidebar' },
            { type: 'link', label: '주소 등록', url: '/shop/cart' },
        ],
    },
    { type: 'button', label: '로그아웃', url: '' },
    {
        type: 'link',
        label: '회원가입',
        url: '/register',
    },

    {
        type: 'link',
        label: '고객센터',
        url: '/site/contact-us',
    },


];
