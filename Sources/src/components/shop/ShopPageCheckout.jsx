// react
import React, {useState, useEffect} from 'react';

// third-party
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link, Redirect } from 'react-router-dom';

// application
import Collapse from '../shared/Collapse';
import Currency from '../shared/Currency';
import PageHeader from '../shared/PageHeader';
import { Check9x7Svg } from '../../svg';
import KakaoPay from '../pay/KakaoPay';
// data stubs
import payments from '../../data/shopPayments';
import theme from '../../data/theme';

// api service
import axios from 'axios';
import * as service from '../../services/member.service'

// 주문 결제 Component
function ShopPageCheckout(props) {

    let { cart, location, history } = props;

    const [ member, setMember ] = useState(null);
    const [ payment, setPayment ] = useState('bank');

    const handlePaymentChange = (event) => {
        if (event.target.checked) {
            this.setState({ payment: event.target.value });
        }
    };

    useEffect(()=>{
      // 상품 조회
      async function fetchData(){
        service.getActiveUser()
          .then(member=> { console.log('member',member); setMember(member) });
      }

      fetchData();
    },[]);
    const renderTotals = () => {
        const cart = props.cart.toJS();
        if (cart.extraLines.length <= 0) {
            return null;
        }

        const extraLines = cart.extraLines.map((extraLine, index) => (
            <tr key={index}>
                <th>{extraLine.title}</th>
                <td><Currency value={extraLine.price} /></td>
            </tr>
        ));

        return (
            <React.Fragment>
                <tbody className="checkout__totals-subtotals">
                {/*
                    <tr>
                        <th>Subtotal</th>
                        <td><Currency value={cart.subtotal} /></td>
                    </tr>

                    {extraLines}
                    */}
                </tbody>
            </React.Fragment>
        );
    }

    const renderCart = () => {
        const cart = props.cart.toJS();
        const order = props.history.location.state.order;
        const orderItem = order.item;

        console.log('props',props);
        console.log('orderItem',orderItem);
        console.log(orderItem.name);
        const items = cart.items.map((item) => (
            <tr key={item.id}>
                <td>{`${item.product.name} × ${item.quantity}`}</td>
                <td><Currency value={item.total} /></td>
            </tr>
        ));

        return (
            <table className="checkout__totals">
                <thead className="checkout__totals-header">
                    <tr>
                        <th>상품</th>
                        <th>상품가격</th>
                    </tr>
                </thead>
                <tbody className="checkout__totals-products">
                    <tr key={orderItem.idx}>
                        <td>{`${orderItem.name} × ${order.quantity}`}</td>
                        <td><Currency value={orderItem.total} /></td>
                    </tr>
                </tbody>
                {renderTotals()}
                <tfoot className="checkout__totals-footer">
                    <tr>
                        <th>총상품가격</th>
                        <td><Currency value={orderItem.total} /></td>
                    </tr>
                </tfoot>
            </table>
        );
    }

    const renderPaymentsList = (payments2) => {
        const { payment: currentPayment } = payment;

        const payments = payments2.map((payment) => {
            const renderPayment = ({ setItemRef, setContentRef }) => (
                <li className="payment-methods__item" ref={setItemRef}>
                    <label className="payment-methods__item-header">
                        <span className="payment-methods__item-radio input-radio">
                            <span className="input-radio__body">
                                <input
                                    type="radio"
                                    className="input-radio__input"
                                    name="checkout_payment_method"
                                    value={payment.key}
                                    checked={currentPayment === payment.key}
                                    onChange={handlePaymentChange}
                                />
                                <span className="input-radio__circle" />
                            </span>
                        </span>
                        <span className="payment-methods__item-title">{payment.title}</span>
                    </label>
                    <div className="payment-methods__item-container" ref={setContentRef}>
                        <div className="payment-methods__item-description text-muted">{payment.description}</div>
                    </div>
                </li>
            );

            return (
                <Collapse
                    key={payment.key}
                    open={currentPayment === payment.key}
                    toggleClass="payment-methods__item--active"
                    render={renderPayment}
                />
            );
        });

        return (
            <div className="payment-methods">
                <ul className="payment-methods__list">
                    {payments}
                </ul>
            </div>
        );
    }

  const order = props.history.location.state.order;
  const orderItem = order.item;
  const orderId = location.pathname.split('/').pop();

  console.log('cart',cart);
  if (cart.get('items').length < 1) {
      return <Redirect to="cart" />;
  }

  const breadcrumb = [
      { title: '상품상세', url: '/shop/cart' },
      { title: '주문/결제', url: '' },
      { title: '주문완료', url: '' }
  ];

  return (
      <React.Fragment>
          <Helmet>
              <title>{`주문/결제 — ${theme.name}`}</title>
          </Helmet>

          <PageHeader header="주문/결제" breadcrumb={breadcrumb} />

          <div className="checkout block">
              <div className="container">
                  <div className="row">
                    {/* 경고창? 같은거 뜨는 부분
                      <div className="col-12 mb-3">
                          <div className="alert alert-primary alert-lg">
                              Returning customer?
                              {' '}
                              <Link to="/login">Click here to login</Link>
                          </div>
                      </div>
                    */}


                      <div className="col-12 col-lg-6 col-xl-7">
                          <div className="card mb-lg-0">
                              <div className="card-body">
                                  <h3 className="card-title">구매자 정보</h3>

                                  <div className="form-group">
                                      <strong>이름</strong>
                                      <br/>
                                      { member ? `${member.name}` : ''}
                                      <br/>
                                      <br/>

                                      <strong>이메일</strong>
                                      <br/>
                                      { member ? `${member.username}` : ''}
                                      <br/>
                                      <br/>

                                      <strong>휴대폰 번호</strong>
                                      <br/>
                                      { member ? `${member.phoneNumber}`.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3') : ''}
                                      <br/>
                                      <br/>
                                  </div>
                                  {/*

                                  <div className="form-group">
                                      <label htmlFor="checkout-country">Country</label>
                                      <select id="checkout-country" className="form-control">
                                          <option>Select a country...</option>
                                          <option>United States</option>
                                          <option>Russia</option>
                                          <option>Italy</option>
                                          <option>France</option>
                                          <option>Ukraine</option>
                                          <option>Germany</option>
                                          <option>Australia</option>
                                      </select>
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="checkout-street-address">Street Address</label>
                                      <input
                                          type="text"
                                          className="form-control"
                                          id="checkout-street-address"
                                          placeholder="Street Address"
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="checkout-address">
                                          Apartment, suite, unit etc.
                                          {' '}
                                          <span className="text-muted">(Optional)</span>
                                      </label>
                                      <input type="text" className="form-control" id="checkout-address" />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="checkout-city">Town / City</label>
                                      <input type="text" className="form-control" id="checkout-city" />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="checkout-state">State / County</label>
                                      <input type="text" className="form-control" id="checkout-state" />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="checkout-postcode">Postcode / ZIP</label>
                                      <input type="text" className="form-control" id="checkout-postcode" />
                                  </div>

                                  <div className="form-row">
                                      <div className="form-group col-md-6">
                                          <label htmlFor="checkout-email">Email address</label>
                                          <input
                                              type="email"
                                              className="form-control"
                                              id="checkout-email"
                                              placeholder="Email address"
                                          />
                                      </div>
                                      <div className="form-group col-md-6">
                                          <label htmlFor="checkout-phone">Phone</label>
                                          <input type="text" className="form-control" id="checkout-phone" placeholder="Phone" />
                                      </div>
                                  </div>

                                  <div className="form-group">
                                      <div className="form-check">
                                          <span className="form-check-input input-check">
                                              <span className="input-check__body">
                                                  <input className="input-check__input" type="checkbox" id="checkout-create-account" />
                                                  <span className="input-check__box" />
                                                  <Check9x7Svg className="input-check__icon" />
                                              </span>
                                          </span>
                                          <label className="form-check-label" htmlFor="checkout-create-account">
                                              Create an account?
                                          </label>
                                      </div>
                                  </div>
                                  */}
                              </div>
                              <div className="card-divider" />
                              <div className="card-body">
                                  <h3 className="card-title">사장님께 부탁</h3>

                                  {/*
                                  <div className="form-group">
                                      <div className="form-check">
                                          <span className="form-check-input input-check">
                                              <span className="input-check__body">
                                                  <input className="input-check__input" type="checkbox" id="checkout-different-address" />
                                                  <span className="input-check__box" />
                                                  <Check9x7Svg className="input-check__icon" />
                                              </span>
                                          </span>
                                          <label className="form-check-label" htmlFor="checkout-different-address">
                                              Ship to a different address?
                                          </label>
                                      </div>
                                  </div>
                                  */}

                                  <div className="form-group">
                                      <label htmlFor="checkout-comment">
                                          사장님께 부탁하기
                                          {' '}
                                          <span className="text-muted">(옵션)</span>
                                      </label>
                                      <textarea id="checkout-comment" className="form-control" rows="4" />
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div className="col-12 col-lg-6 col-xl-5 mt-4 mt-lg-0">
                          <div className="card mb-0">
                              <div className="card-body">
                                  <h3 className="card-title">결제정보</h3>

                                  {renderCart()}

                                  {renderPaymentsList(payments)}

                                  <div className="checkout__agree form-group">
                                      <div className="form-check">
                                          <span className="form-check-input input-check">
                                              <span className="input-check__body">
                                                  <input className="input-check__input" type="checkbox" id="checkout-terms" />
                                                  <span className="input-check__box" />
                                                  <Check9x7Svg className="input-check__icon" />
                                              </span>
                                          </span>
                                          <label className="form-check-label" htmlFor="checkout-terms">
                                              구매 조건 확인 및 결제대행 서비스 약관 동의
                                              <Link to="site/terms">보기</Link>

                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div className="card-divider" />
                              <div className="card-body">
                                  <h4 className="card-title">현금영수증</h4>

                                    <div className="form-group cash_receipt">
                                        휴대폰번호 : <b>{ member ? `${member.phoneNumber}`.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3') : ''}</b> (소득공제)
                                    </div>

                                  <KakaoPay orderId={orderId} itemname={orderItem.name} amount={orderItem.total} history={history} />
                              </div>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </React.Fragment>
    )
}


const mapStateToProps = (state) => ({
    cart: state.cart
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ShopPageCheckout);
