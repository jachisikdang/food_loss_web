import { createAction, handleActions } from 'redux-actions'
import { Map } from 'immutable'

// Actions
const SIDEBAR_CLOSE = 'sidebar/SIDEBAR_CLOSE';
const SIDEBAR_OPEN = 'sidebar/SIDEBAR_OPEN';

// Actions Creators
export const sidebarClose = createAction(SIDEBAR_CLOSE); //  { form, name, value }
export const sidebarOpen = createAction(SIDEBAR_OPEN); // form

const initialState = Map({
    open: false,
});

export default handleActions({
  [SIDEBAR_OPEN]: (state,action) => {
    return state.set('open',true);
  },
  [SIDEBAR_CLOSE]: (state,action) => {
    return state.set('open',false);
  }
},initialState);
